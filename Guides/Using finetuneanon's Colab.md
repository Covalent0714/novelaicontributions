The following is a guide aimed at making it as easy and clear as possible to use [finetuneanon's GPT-NEO Dungeon](https://github.com/finetuneanon/gpt-neo_dungeon) Google Colab. If you don't know what this is or are somehow still having problems after following the guide's instructions, [visit the FAQ](#faq).

## Getting Started
There are a few steps you can take to better prepare ahead of time and save yourself some frustration later.
- If you don't have a Google Drive account, sign up for one [here](https://www.google.com/drive/). No, using someone's Google Colab doesn't leak all your personal information.
- 

...under construction


## FAQ
**Q)** What's this?

**A)** There's an open-source autoregressive language model called GPT-NEO which people are using for text adventures and literature co-authorship. To get more use out of GPT-NEO, users can finetune it with additional training data to create a model that produces more specifically curated output. Finetuneanon has created two such models dubbed "horni" (which is tuned with random passages from literotica) and "horni-ln" (tuned with some light novel content).
Normally models like these need to be hosted online somewhere (such as the case with AI Dungeon) or run locally on high-powered computers, but luckily Google provides its Colab service to freely host and run small applications like these up to a limited size. Thus, finetuneanon's Colab can function like a poor-anon's AI Dungeon or similar application, but with custom finetuned models.

**Q)** 

**A)** 

**Q)** 

**A)** 

**Q)** 

**A)** 
