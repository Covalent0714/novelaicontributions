# NovelAI Community Repository
An early community contribution for the NovelAI project

## About this repository
It is an attempt to summarize and centralize community contributions for NovelAI  
It is maintained by Noli, Lion and Javaman, which are not official part of the NovelAI team  

## Who is this repo meant for
This repo is meant for anyone that wants to contribute to the community effort in the NovelAI project

## I want to contribute
Good! You can clone this repo and make a Merge Request (MR), We will be happy to get some activity here!

## Merge Request lifecycle
Once you send a Merge Request, it has to be approved by at least one maintainer before it can be merged  
You can send an MR even if the branch isn't finished yet, just append `Draft:` or `WIP:` to the MR name to block accidental merge until it's finished  
Sending your MR in `Draft` or `WIP` state is useful to get feedbacks earlier and avoid going too far into wrong paths

#### MR Labels
To help visualise MR states, labels are present to indicate some info:
- ~"MR::Need review" A review is needed, if no label is present, this is the default state
- ~"MR::Review OK" A review has been done with no problem found, requester can set the ~"MR::Ready to merge" label if they need it merged
- ~"MR::Fix needed" A review has been done, but they are problems to fix, requester should fix them then put the ~"MR::Need review" back
- ~"MR::Ready to merge" The branch is ready to be merged, but requires a review and approval in any case

#### MR Approval
Merge Requests need to be approved by at least one `approver` or `reviewer`, generally a repo maintainer  

#### MR Approval for maintainers
Maintainers cannot approve their own MR (Noli included), this is to enforce a validation by another maintainer  
They can however merge their own MR if it has been approved by another maintainer  
These mesures are necessary to keep everything as tidy and collaborative as possible  
Feel free to debate those decisions if you think we could do better otherwise

## On a side note
Note to official NovelAI team members that may read this, you are welcome on this repo. Collaboration and communication between the community collaborators and the official staff is important, the sooner the better
